# Project based on ZendSkeletonApplication

## Introduction

This application is using Zend Framework MVC.
It is used to compare two repositories from GitHub

## Installation

After download make composer install

### Api request

Example: http:/localhost/api/compare/repository?first_repository=zfcampus/zf-rest&second_repository=https://github.com/dtrupenn/Tetris

## Web server setup

### Local PHP run

You can run application using local PHP server

```php
php -S 0.0.0.0:8080 -t public public/index.php
```

or run composer run-script

```composer
composer run-script serve
```

### Apache setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

```apache
<VirtualHost *:80>
    ServerName git-zend
    DirectoryIndex index.php
    DocumentRoot /path/to/github-zend/public/
    <Directory /path/to/github-zend/public/">
       Order allow,deny
       Allow from all
       Require all granted
        <IfModule mod_rewrite.c>
            Options -MultiViews
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^(.*)$ index.php [QSA,L]
        </IfModule>
    </Directory>
    ErrorLog "logs/local.github-zend-error.log"
    CustomLog "logs/local.github-zend.log" common
</VirtualHost>
```
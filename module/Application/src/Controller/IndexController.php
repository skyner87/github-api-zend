<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Form\ComparisonForm;
use Application\Manager\Exception\RepositoryNotFoundException;
use Application\Manager\GitHubRepositoryComparatorManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    /**
     * @var GitHubRepositoryComparatorManager
     */
    protected $comparatorManager;

    /**
     * IndexController constructor.
     * @param GitHubRepositoryComparatorManager $comparatorManager
     */
    public function __construct(GitHubRepositoryComparatorManager $comparatorManager)
    {
        $this->comparatorManager = $comparatorManager;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function indexAction()
    {
        $compareRepositoriesResult = null;

        if ($this->getRequest()->isPost()) {
            $firstRepository = $this->getRequest()->getPost('first_repository');
            $secondRepository = $this->getRequest()->getPost('second_repository');

            try {
                $this->comparatorManager->addRepository($firstRepository);
                $this->comparatorManager->addRepository($secondRepository);
                $compareRepositoriesResult =
                    $this->comparatorManager->compareRepositoriesAndReturnCompareRepositoriesResultModel();
            } catch (RepositoryNotFoundException $repositoryNotFoundException) {
                $this->flashMessenger()->addErrorMessage($repositoryNotFoundException->getMessage());
                return $this->redirect()->toRoute('home');
            } catch (\Exception $exception) {
                $this->flashMessenger()->addErrorMessage('Please contact with administrator');
                return $this->redirect()->toRoute('home');
            }
        }

        return new ViewModel(
            [
                'comparisonForm' => new ComparisonForm(),
                'compareRepositoriesResult' => $compareRepositoriesResult
            ]
        );
    }
}

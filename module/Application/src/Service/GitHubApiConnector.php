<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-21
 * Time: 19:35
 */

namespace Application\Service;

use Application\Service\GitHubApiConnector\Exception\JsonDecodeException;
use Application\Service\GitHubApiConnector\Exception\RepositoryException;
use Application\Service\GitHubApiConnector\Exception\NotFoundException;
use Application\Service\GitHubApiConnector\Model\RepositoryModel;
use Application\Service\GitHubApiConnector\Model\RepositoryReleaseModel;
use Zend\Http\Client;
use Zend\Http\Header\Accept;
use Zend\Http\Headers;
use Zend\Hydrator\ClassMethods;

class GitHubApiConnector
{
    /**
     * @var string
     */
    protected $apiUrl;

    /**
     * @var string
     */
    protected $apiLogin;

    /**
     * @var string
     */
    protected $apiPassword;

    /**
     * GitHubApiConnector constructor.
     * @param string $apiUrl
     * @param string $apiLogin
     * @param string $apiPassword
     */
    public function __construct(string $apiUrl, string $apiLogin, string $apiPassword)
    {
        $this->apiUrl = $apiUrl;
        $this->apiLogin = $apiLogin;
        $this->apiPassword = $apiPassword;
    }

    /**
     * @param string $fullName
     * @return RepositoryModel
     * @throws \Exception
     */
    public function getRepository(string $fullName): RepositoryModel
    {
        try {
            $object = $this->apiCall('repos' . '/' . $fullName);
            $repositoryModel = new RepositoryModel();
            $hydrator = new ClassMethods();
            $hydrator->hydrate((array)$object, $repositoryModel);
            return $repositoryModel;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param string $fullName
     * @return RepositoryReleaseModel|null
     * @throws \Exception
     */
    public function getRepositoryRelease(string $fullName): ?RepositoryReleaseModel
    {
        try {
            $object = $this->apiCall('repos' . '/' . $fullName . '/releases/latest');
            $repositoryReleaseModel = new RepositoryReleaseModel();
            $hydrator = new ClassMethods();
            $hydrator->hydrate((array)$object, $repositoryReleaseModel);
            return $repositoryReleaseModel;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param $path
     * @param string $method
     * @return mixed
     * @throws \Exception
     */
    protected function apiCall($path, $method = 'GET')
    {
        $requestedUrl = $this->apiUrl . '/' . $path;
        $zendHttpClient = new Client($requestedUrl);
        $zendHttpClient->setAuth($this->apiLogin, $this->apiPassword);
        $zendHttpClient->setMethod($method);
        $zendHeaders = new Headers();
        $headerAccept = new Accept();
        $headerAccept->parseHeaderLine('Accept: application/vnd.github.v3+json');
        $zendHeaders->addHeader($headerAccept);
        $zendHttpClient->setHeaders($zendHeaders);

        try {
            $response = $zendHttpClient->send();

            if ($response->getStatusCode() === 200) {
                return $this->returnJsonDecode($response->getBody());
            } elseif ($response->getStatusCode() === 404) {
                throw new NotFoundException();
            } else {
                $errorObject = $this->returnJsonDecode($response->getBody());
                $message = 'GitHub API Error';
                if (property_exists($errorObject, 'message')) {
                    $message = $errorObject->message;
                }
                throw new RepositoryException($message, $response->getStatusCode());
            }
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param string $json
     * @return mixed
     * @throws JsonDecodeException
     */
    private function returnJsonDecode(string $json)
    {
        $decodedJson = json_decode($json);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new JsonDecodeException(json_last_error());
        }

        return $decodedJson;
    }
}

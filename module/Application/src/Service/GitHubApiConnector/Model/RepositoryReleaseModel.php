<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-21
 * Time: 22:22
 */

namespace Application\Service\GitHubApiConnector\Model;

class RepositoryReleaseModel
{
    /**
     * @var null|string
     */
    public $publishedAt;

    /**
     * @return null|string
     */
    public function getPublishedAt(): ?string
    {
        return $this->publishedAt;
    }

    /**
     * @param null|string $publishedAt
     * @return RepositoryReleaseModel
     */
    public function setPublishedAt(?string $publishedAt): RepositoryReleaseModel
    {
        $this->publishedAt = $publishedAt;
        return $this;
    }
}

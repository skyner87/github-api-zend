<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-21
 * Time: 20:36
 */

namespace Application\Service\GitHubApiConnector\Model;

class RepositoryModel
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $nodeId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $fullName;

    /**
     * @var bool
     */
    public $private;

    /**
     * @var string
     */
    public $htmlUrl;

    /**
     * @var string
     */
    public $description;

    /**
     * @var int
     */
    public $fork;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $homepage;

    /**
     * @var string
     */
    public $string;

    /**
     * @var int
     */
    public $size;

    /**
     * @var int
     */
    public $stargazersCount;

    /**
     * @var int
     */
    public $watchersCount;

    /**
     * @var string
     */
    public $language;

    /**
     * @var bool
     */
    public $hasProjects;

    /**
     * @var bool
     */
    public $hasDownloads;

    /**
     * @var bool
     */
    public $hasWiki;

    /**
     * @var bool
     */
    public $hasPages;

    /**
     * @var int
     */
    public $forksCount;

    /**
     * @var int
     */
    public $openIssuesCount;

    public $forks;

    /**
     * @var int
     */
    public $openIssues;

    /**
     * @var int
     */
    public $watchers;

    /**
     * @var bool
     */
    public $defaultBranch;

    /**
     * @var int
     */
    public $networkCount;

    /**
     * @var int
     */
    public $subscribersCount;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNodeId()
    {
        return $this->nodeId;
    }

    /**
     * @param mixed $nodeId
     */
    public function setNodeId($nodeId)
    {
        $this->nodeId = $nodeId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * @param mixed $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return mixed
     */
    public function getHtmlUrl()
    {
        return $this->htmlUrl;
    }

    /**
     * @param mixed $htmlUrl
     */
    public function setHtmlUrl($htmlUrl)
    {
        $this->htmlUrl = $htmlUrl;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getFork()
    {
        return $this->fork;
    }

    /**
     * @param mixed $fork
     */
    public function setFork($fork)
    {
        $this->fork = $fork;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * @param mixed $homepage
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @return mixed
     */
    public function getString()
    {
        return $this->string;
    }

    /**
     * @param mixed $string
     */
    public function setString($string)
    {
        $this->string = $string;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getStargazersCount()
    {
        return $this->stargazersCount;
    }

    /**
     * @param mixed $stargazersCount
     */
    public function setStargazersCount($stargazersCount)
    {
        $this->stargazersCount = $stargazersCount;
    }

    /**
     * @return mixed
     */
    public function getWatchersCount()
    {
        return $this->watchersCount;
    }

    /**
     * @param mixed $watchersCount
     */
    public function setWatchersCount($watchersCount)
    {
        $this->watchersCount = $watchersCount;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getHasProjects()
    {
        return $this->hasProjects;
    }

    /**
     * @param mixed $hasProjects
     */
    public function setHasProjects($hasProjects)
    {
        $this->hasProjects = $hasProjects;
    }

    /**
     * @return mixed
     */
    public function getHasDownloads()
    {
        return $this->hasDownloads;
    }

    /**
     * @param mixed $hasDownloads
     */
    public function setHasDownloads($hasDownloads)
    {
        $this->hasDownloads = $hasDownloads;
    }

    /**
     * @return mixed
     */
    public function getHasWiki()
    {
        return $this->hasWiki;
    }

    /**
     * @param mixed $hasWiki
     */
    public function setHasWiki($hasWiki)
    {
        $this->hasWiki = $hasWiki;
    }

    /**
     * @return mixed
     */
    public function getHasPages()
    {
        return $this->hasPages;
    }

    /**
     * @param mixed $hasPages
     */
    public function setHasPages($hasPages)
    {
        $this->hasPages = $hasPages;
    }

    /**
     * @return mixed
     */
    public function getForksCount()
    {
        return $this->forksCount;
    }

    /**
     * @param mixed $forksCount
     */
    public function setForksCount($forksCount)
    {
        $this->forksCount = $forksCount;
    }

    /**
     * @return mixed
     */
    public function getOpenIssuesCount()
    {
        return $this->openIssuesCount;
    }

    /**
     * @param mixed $openIssuesCount
     */
    public function setOpenIssuesCount($openIssuesCount)
    {
        $this->openIssuesCount = $openIssuesCount;
    }

    /**
     * @return mixed
     */
    public function getForks()
    {
        return $this->forks;
    }

    /**
     * @param mixed $forks
     */
    public function setForks($forks)
    {
        $this->forks = $forks;
    }

    /**
     * @return mixed
     */
    public function getOpenIssues()
    {
        return $this->openIssues;
    }

    /**
     * @param mixed $openIssues
     */
    public function setOpenIssues($openIssues)
    {
        $this->openIssues = $openIssues;
    }

    /**
     * @return mixed
     */
    public function getWatchers()
    {
        return $this->watchers;
    }

    /**
     * @param mixed $watchers
     */
    public function setWatchers($watchers)
    {
        $this->watchers = $watchers;
    }

    /**
     * @return mixed
     */
    public function getDefaultBranch()
    {
        return $this->defaultBranch;
    }

    /**
     * @param mixed $defaultBranch
     */
    public function setDefaultBranch($defaultBranch)
    {
        $this->defaultBranch = $defaultBranch;
    }

    /**
     * @return mixed
     */
    public function getNetworkCount()
    {
        return $this->networkCount;
    }

    /**
     * @param mixed $networkCount
     */
    public function setNetworkCount($networkCount)
    {
        $this->networkCount = $networkCount;
    }

    /**
     * @return mixed
     */
    public function getSubscribersCount()
    {
        return $this->subscribersCount;
    }

    /**
     * @param mixed $subscribersCount
     */
    public function setSubscribersCount($subscribersCount)
    {
        $this->subscribersCount = $subscribersCount;
    }
}

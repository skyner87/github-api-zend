<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-21
 * Time: 19:35
 */

namespace Application\Service\Factory;

use Application\Service\GitHubApiConnector;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class GitHubApiConnectorFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return GitHubApiConnector|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $gitHubConfig = $config['gitHubApiConfig'];
        $apiConnector =
            new GitHubApiConnector($gitHubConfig['api_url'], $gitHubConfig['api_login'], $gitHubConfig['api_password']);
        return $apiConnector;
    }
}

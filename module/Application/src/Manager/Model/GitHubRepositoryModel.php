<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-21
 * Time: 22:55
 */

namespace Application\Manager\Model;

class GitHubRepositoryModel
{
    /**
     * @var string
     */
    public $fullName;

    /**
     * @var string
     */
    public $htmlUrl;

    /**
     * @var int
     */
    public $stargazersCount;

    /**
     * @var int
     */
    public $watchersCount;

    /**
     * @var int
     */
    public $forksCount;

    /**
     * @var int
     */
    public $openIssuesCount;

    /**
     * @var int
     */
    public $subscribersCount;

    /**
     * @var bool
     */
    public $hasWiki;

    /**
     * @var null|string
     */
    public $publishedAt;

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     * @return GitHubRepositoryModel
     */
    public function setFullName(string $fullName): GitHubRepositoryModel
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getHtmlUrl(): string
    {
        return $this->htmlUrl;
    }

    /**
     * @param string $htmlUrl
     * @return GitHubRepositoryModel
     */
    public function setHtmlUrl(string $htmlUrl): GitHubRepositoryModel
    {
        $this->htmlUrl = $htmlUrl;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStargazersCount()
    {
        return $this->stargazersCount;
    }

    /**
     * @param mixed $stargazersCount
     * @return GitHubRepositoryModel
     */
    public function setStargazersCount($stargazersCount)
    {
        $this->stargazersCount = $stargazersCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getWatchersCount(): int
    {
        return $this->watchersCount;
    }

    /**
     * @param int $watchersCount
     * @return GitHubRepositoryModel
     */
    public function setWatchersCount(int $watchersCount): GitHubRepositoryModel
    {
        $this->watchersCount = $watchersCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getForksCount(): int
    {
        return $this->forksCount;
    }

    /**
     * @param int $forksCount
     * @return GitHubRepositoryModel
     */
    public function setForksCount(int $forksCount): GitHubRepositoryModel
    {
        $this->forksCount = $forksCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getOpenIssuesCount(): int
    {
        return $this->openIssuesCount;
    }

    /**
     * @param int $openIssuesCount
     * @return GitHubRepositoryModel
     */
    public function setOpenIssuesCount(int $openIssuesCount): GitHubRepositoryModel
    {
        $this->openIssuesCount = $openIssuesCount;
        return $this;
    }

    /**
     * @return int
     */
    public function getSubscribersCount(): int
    {
        return $this->subscribersCount;
    }

    /**
     * @param int $subscribersCount
     * @return GitHubRepositoryModel
     */
    public function setSubscribersCount(int $subscribersCount): GitHubRepositoryModel
    {
        $this->subscribersCount = $subscribersCount;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHasWiki(): bool
    {
        return $this->hasWiki;
    }

    /**
     * @param bool $hasWiki
     * @return GitHubRepositoryModel
     */
    public function setHasWiki(bool $hasWiki): GitHubRepositoryModel
    {
        $this->hasWiki = $hasWiki;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getPublishedAt(): ?string
    {
        return $this->publishedAt;
    }

    /**
     * @param null|string $publishedAt
     * @return GitHubRepositoryModel
     */
    public function setPublishedAt(?string $publishedAt): GitHubRepositoryModel
    {
        $this->publishedAt = $publishedAt;
        return $this;
    }
}

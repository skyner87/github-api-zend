<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-21
 * Time: 22:10
 */

namespace Application\Manager\Model;

class CompareRepositoriesResultModel
{
    /**
     * @var array
     */
    protected $repositoryName = [];

    /**
     * @var array
     */
    protected $repositoryUrl = [];

    /**
     * @var array
     */
    protected $comparisonArray = [];

    /**
     * @param $firstRepositoryName
     * @param $secondRepositoryName
     */
    public function setRepositoryNames($firstRepositoryName, $secondRepositoryName): void
    {
        $this->repositoryName = [$firstRepositoryName, $secondRepositoryName];
    }

    /**
     * @param $firstRepositoryUrl
     * @param $secondRepositoryUrl
     */
    public function setRepositoryUrls($firstRepositoryUrl, $secondRepositoryUrl): void
    {
        $this->repositoryUrl = [$firstRepositoryUrl, $secondRepositoryUrl];
    }

    /**
     * @param $field
     * @param $firstRepositoryValue
     * @param $secondRepositoryValue
     */
    public function addComparison($field, $firstRepositoryValue, $secondRepositoryValue): void
    {
        $this->comparisonArray[$field] = [$firstRepositoryValue, $secondRepositoryValue];
    }

    /**
     * @return array
     */
    public function getRepositoryName()
    {
        return $this->repositoryName;
    }

    /**
     * @return array
     */
    public function getRepositoryUrl()
    {
        return $this->repositoryUrl;
    }

    /**
     * @return array
     */
    public function getComparisonArray()
    {
        return $this->comparisonArray;
    }

    /**
     * @param array $comparisonArray
     */
    public function setComparisonArray($comparisonArray)
    {
        $this->comparisonArray = $comparisonArray;
    }
}

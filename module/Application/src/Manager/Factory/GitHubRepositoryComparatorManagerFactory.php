<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-21
 * Time: 21:32
 */

namespace Application\Manager\Factory;

use Application\Manager\GitHubRepositoryComparatorManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class GitHubRepositoryComparatorManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return GitHubRepositoryComparatorManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $manager = new GitHubRepositoryComparatorManager(
            $container->get(\Application\Service\GitHubApiConnector::class)
        );
        return $manager;
    }
}

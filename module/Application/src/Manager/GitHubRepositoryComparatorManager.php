<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-21
 * Time: 21:21
 */

namespace Application\Manager;

use Application\Manager\Exception\GetRepositoryException;
use Application\Manager\Exception\RepositoryNotFoundException;
use Application\Manager\Exception\TooManyRepositoriesException;
use Application\Manager\Exception\WrongNumberOfRepositoriesToCompare;
use Application\Manager\Model\CompareRepositoriesResultModel;
use Application\Manager\Model\GitHubRepositoryModel;
use Application\Service\GitHubApiConnector;
use Zend\Hydrator\ClassMethods;
use Zend\Hydrator\Reflection;

class GitHubRepositoryComparatorManager
{
    /**
     * @var array
     */
    protected $fieldsToCompare = [
        'stargazersCount',
        'watchersCount',
        'forksCount',
        'openIssuesCount',
        'subscribersCount',
        'hasWiki',
        'publishedAt',
    ];

    /**
     * @var GitHubApiConnector
     */
    protected $gitHubApiConnector;

    /**
     * @var array
     */
    protected $repositoryArray = [];

    /**
     * GitHubRepositoryComparatorManager constructor.
     * @param GitHubApiConnector $gitHubApiConnector
     */
    public function __construct(GitHubApiConnector $gitHubApiConnector)
    {
        $this->gitHubApiConnector = $gitHubApiConnector;
    }

    /**
     * @param string $repository
     * @throws GetRepositoryException
     * @throws RepositoryNotFoundException
     * @throws TooManyRepositoriesException
     */
    public function addRepository(string $repository): void
    {
        if ($this->checkIfGitHubHtmlRepositoryUrl($repository)) {
            $repository = $this->getRepositoryNameFromGithubHtmlRepositoryUrl($repository);
        }

        if (count($this->repositoryArray) >= 2) {
            throw new TooManyRepositoriesException('Allowed repository to compare is 2');
        }

        try {
            $gitHubRepositoryModel = new GitHubRepositoryModel();

            $reflectionHydrator = new Reflection();
            $reflectedData = $reflectionHydrator->extract($this->gitHubApiConnector->getRepository($repository));

            $hydrator = new ClassMethods();
            $hydrator->hydrate($reflectedData, $gitHubRepositoryModel);

            $this->addPublishedAtToModel($gitHubRepositoryModel, $repository);

            $this->repositoryArray[] = $gitHubRepositoryModel;
        } catch (GitHubApiConnector\Exception\NotFoundException $notFoundException) {
            throw new RepositoryNotFoundException(sprintf('Repository %s not found', $repository));
        } catch (GitHubApiConnector\Exception\RepositoryException $repositoryException) {
            throw new GetRepositoryException($repositoryException->getMessage(), $repositoryException->getCode());
        }
    }

    /**
     * @param GitHubRepositoryModel $gitHubRepositoryModel
     * @param string $repository
     * @throws GetRepositoryException
     */
    public function addPublishedAtToModel(GitHubRepositoryModel $gitHubRepositoryModel, string $repository) : void
    {
        try {
            $repositoryRelease = $this->gitHubApiConnector->getRepositoryRelease($repository);
            if ($repositoryRelease instanceof GitHubApiConnector\Model\RepositoryReleaseModel) {
                $gitHubRepositoryModel->setPublishedAt(
                    $this->gitHubApiConnector->getRepositoryRelease($repository)->getPublishedAt()
                );
            }
        } catch (GitHubApiConnector\Exception\NotFoundException $exception) {
        } catch (\Exception $exception) {
            throw new GetRepositoryException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param string $repository
     * @return bool
     */
    private function checkIfGitHubHtmlRepositoryUrl(string $repository): bool
    {
        return (bool)preg_match('/github.com/', $repository);
    }

    /**
     * @param string $repository
     * @return string
     */
    private function getRepositoryNameFromGithubHtmlRepositoryUrl(string $repository): string
    {
        return preg_replace(
            '/^github.com\//',
            '',
            substr($repository, strpos($repository, 'github.com'), strlen($repository))
        );
    }

    /**
     * @return CompareRepositoriesResultModel
     * @throws WrongNumberOfRepositoriesToCompare
     */
    public function compareRepositoriesAndReturnCompareRepositoriesResultModel(): CompareRepositoriesResultModel
    {
        $repositoryCompareModel = new CompareRepositoriesResultModel();

        if (count($this->repositoryArray) !== 2) {
            throw new WrongNumberOfRepositoriesToCompare();
        }

        /* @var $firstRepository \Application\Manager\Model\GitHubRepositoryModel */
        $firstRepository = $this->repositoryArray[0];
        /* @var $secondRepository \Application\Manager\Model\GitHubRepositoryModel */
        $secondRepository = $this->repositoryArray[1];

        $repositoryCompareModel->setRepositoryNames($firstRepository->getFullName(), $secondRepository->getFullName());
        $repositoryCompareModel->setRepositoryUrls($firstRepository->getHtmlUrl(), $secondRepository->getHtmlUrl());

        foreach ($this->fieldsToCompare as $field) {
            if (isset($firstRepository->$field) || $secondRepository->$field) {
                $repositoryCompareModel->addComparison($field, $firstRepository->$field, $secondRepository->$field);
            }
        }

        return $repositoryCompareModel;
    }
}

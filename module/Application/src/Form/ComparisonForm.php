<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-21
 * Time: 23:50
 */

namespace Application\Form;

use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class ComparisonForm extends Form
{
    /**
     * ComparisonForm constructor.
     * @param null $name
     * @param array $options
     */
    public function __construct($name = null, array $options = [])
    {
        parent::__construct($name, $options);

        $firstRepository = new Text('first_repository');
        $firstRepository->setLabel('First repository');
        $this->add($firstRepository);

        $secondRepository = new Text('second_repository');
        $secondRepository->setLabel('Second repository');
        $this->add($secondRepository);

        $submit = new Submit('submit');
        $submit->setLabel('Compare');
        $submit->setValue('Compare');
        $this->add($submit);
    }
}

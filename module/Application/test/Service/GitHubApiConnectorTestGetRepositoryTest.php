<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-10-15
 * Time: 21:44
 */

namespace ApplicationTest\Service;

use Application\Service\GitHubApiConnector;
use PHPUnit\Framework\TestCase;

class GitHubApiConnectorTestGetRepositoryTest extends TestCase
{
    public function testGetRepository()
    {
        $gitHubApiConfig = [
            'api_url' => 'https://api.github.com',
            'api_login' => 'lehehurify1',
            'api_password' => 'janKowalski1'
        ];

        $githubConnector = new GitHubApiConnector(
            $gitHubApiConfig['api_url'],
            $gitHubApiConfig['api_login'],
            $gitHubApiConfig['api_password']
        );

        $repository = $githubConnector->getRepository('magento/magento2');

        $this->assertInstanceOf(GitHubApiConnector\Model\RepositoryModel::class, $repository);
        $this->assertObjectHasAttribute('id', $repository);
        $this->assertObjectHasAttribute('nodeId', $repository);
        $this->assertObjectHasAttribute('name', $repository);
        $this->assertObjectHasAttribute('fullName', $repository);
        $this->assertObjectHasAttribute('htmlUrl', $repository);
        $this->assertObjectHasAttribute('description', $repository);
        $this->assertObjectHasAttribute('fork', $repository);
        $this->assertObjectHasAttribute('url', $repository);
        $this->assertObjectHasAttribute('homepage', $repository);
        $this->assertObjectHasAttribute('string', $repository);
        $this->assertObjectHasAttribute('size', $repository);
        $this->assertObjectHasAttribute('stargazersCount', $repository);
        $this->assertObjectHasAttribute('watchersCount', $repository);
        $this->assertObjectHasAttribute('language', $repository);
        $this->assertObjectHasAttribute('hasProjects', $repository);
        $this->assertObjectHasAttribute('hasDownloads', $repository);
        $this->assertObjectHasAttribute('hasWiki', $repository);
        $this->assertObjectHasAttribute('hasPages', $repository);
        $this->assertObjectHasAttribute('forksCount', $repository);
        $this->assertObjectHasAttribute('openIssuesCount', $repository);
        $this->assertObjectHasAttribute('forks', $repository);
        $this->assertObjectHasAttribute('openIssues', $repository);
        $this->assertObjectHasAttribute('watchers', $repository);
        $this->assertObjectHasAttribute('defaultBranch', $repository);
        $this->assertObjectHasAttribute('networkCount', $repository);
        $this->assertObjectHasAttribute('subscribersCount', $repository);
    }

    /**
     * @expectedException Application\Service\GitHubApiConnector\Exception\NotFoundException
     */
    public function testGetRepositoryExpectedNotFoundException()
    {
        $gitHubApiConfig = [
            'api_url' => 'https://api.github.com',
            'api_login' => 'lehehurify1',
            'api_password' => 'janKowalski1'
        ];

        $githubConnector = new GitHubApiConnector(
            $gitHubApiConfig['api_url'],
            $gitHubApiConfig['api_login'],
            $gitHubApiConfig['api_password']
        );

        $githubConnector->getRepository('dsadas/magento/magento2');
    }

    /**
     * @expectedException Application\Service\GitHubApiConnector\Exception\RepositoryException
     */
    public function testGetRepositoryExpectedRepositoryException()
    {
        $gitHubApiConfig = [
            'api_url' => 'https://api.github.com',
            'api_login' => 'lehehuridsafy1',
            'api_password' => 'janKowadsalski1'
        ];

        $githubConnector = new GitHubApiConnector(
            $gitHubApiConfig['api_url'],
            $gitHubApiConfig['api_login'],
            $gitHubApiConfig['api_password']
        );

        $githubConnector->getRepository('dsadas/magento/magento2');
    }

    public function testGetRepositoryRelease()
    {
        $gitHubApiConfig = [
            'api_url' => 'https://api.github.com',
            'api_login' => 'lehehurify1',
            'api_password' => 'janKowalski1'
        ];

        $githubConnector = new GitHubApiConnector(
            $gitHubApiConfig['api_url'],
            $gitHubApiConfig['api_login'],
            $gitHubApiConfig['api_password']
        );

        $repositoryRelease = $githubConnector->getRepositoryRelease('magento/magento2');

        $this->assertInstanceOf(GitHubApiConnector\Model\RepositoryReleaseModel::class, $repositoryRelease);
        $this->assertObjectHasAttribute('publishedAt', $repositoryRelease);
    }

    /**
     * @expectedException Application\Service\GitHubApiConnector\Exception\NotFoundException
     */
    public function testGetRepositoryReleaseExpectedNotFoundException()
    {
        $gitHubApiConfig = [
            'api_url' => 'https://api.github.com',
            'api_login' => 'lehehurify1',
            'api_password' => 'janKowalski1'
        ];

        $githubConnector = new GitHubApiConnector(
            $gitHubApiConfig['api_url'],
            $gitHubApiConfig['api_login'],
            $gitHubApiConfig['api_password']
        );

        $githubConnector->getRepositoryRelease('dsadas/magento/magento2');
    }

    /**
     * @expectedException Application\Service\GitHubApiConnector\Exception\RepositoryException
     */
    public function testGetRepositoryReleaseExpectedRepositoryException()
    {
        $gitHubApiConfig = [
            'api_url' => 'https://api.github.com',
            'api_login' => 'lehehuridsafy1',
            'api_password' => 'janKowadsalski1'
        ];

        $githubConnector = new GitHubApiConnector(
            $gitHubApiConfig['api_url'],
            $gitHubApiConfig['api_login'],
            $gitHubApiConfig['api_password']
        );

        $githubConnector->getRepositoryRelease('dsadas/magento/magento2');
    }
}

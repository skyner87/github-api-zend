<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-10-23
 * Time: 21:33
 */

namespace ApplicationTest\Manager;

use Application\Manager\Exception\TooManyRepositoriesException;
use Application\Manager\GitHubRepositoryComparatorManager;
use Application\Manager\Model\CompareRepositoriesResultModel;
use Application\Manager\Model\GitHubRepositoryModel;
use Application\Service\GitHubApiConnector;
use PHPUnit\Framework\TestCase;

class GitHubRepositoryComparatorManagerTest extends TestCase
{
    /**
     * @var GitHubApiConnector
     */
    protected $gitHubApiConnector;

    public function setUp()
    {
        $gitHubApiConfig = [
            'api_url' => 'https://api.github.com',
            'api_login' => 'lehehurify1',
            'api_password' => 'janKowalski1'
        ];

        $this->gitHubApiConnector = new GitHubApiConnector(
            $gitHubApiConfig['api_url'],
            $gitHubApiConfig['api_login'],
            $gitHubApiConfig['api_password']
        );
    }

    /**
     * @expectedException Application\Manager\Exception\TooManyRepositoriesException
     */
    public function testAddRepositoryTooManyRepositoriesException()
    {
        $githubManager = new GitHubRepositoryComparatorManager($this->gitHubApiConnector);

        $githubManager->addRepository('https://github.com/docker-library/php');
        $githubManager->addRepository('chef-cookbooks/php');
        $githubManager->addRepository('https://github.com/GiggleAll/php');
    }

    /**
     * @expectedException Application\Manager\Exception\RepositoryNotFoundException
     */
    public function testAddRepositoryRepositoryNotFoundException()
    {
        $githubManager = new GitHubRepositoryComparatorManager($this->gitHubApiConnector);

        $githubManager->addRepository('https://github.com/docker-library/phdsadsasp');
    }

    /**
     * @expectedException Application\Manager\Exception\GetRepositoryException
     */
    public function testAddRepositoryRepositoryException()
    {
        $gitHubApiConfig = [
            'api_url' => 'https://api.github.com',
            'api_login' => 'lehehurify1dsa',
            'api_password' => 'janKowalski1dsa'
        ];

        $gitHubApiConnector = new GitHubApiConnector(
            $gitHubApiConfig['api_url'],
            $gitHubApiConfig['api_login'],
            $gitHubApiConfig['api_password']
        );

        $githubManager = new GitHubRepositoryComparatorManager($gitHubApiConnector);

        $githubManager->addRepository('https://github.com/docker-library/phdsadsasp');
    }

    /**
     * @expectedException  \Application\Manager\Exception\GetRepositoryException
     */
    public function testAddPublishedAtToModelThrowsException()
    {
        $gitHubApiConfig = [
            'api_url' => 'https://api.github.com',
            'api_login' => 'lehehurify1dsa',
            'api_password' => 'janKowalski1dsa'
        ];

        $gitHubApiConnector = new GitHubApiConnector(
            $gitHubApiConfig['api_url'],
            $gitHubApiConfig['api_login'],
            $gitHubApiConfig['api_password']
        );

        $githubManager = new GitHubRepositoryComparatorManager($gitHubApiConnector);

        $repositoryModel = new GitHubRepositoryModel();

        $githubManager->addPublishedAtToModel($repositoryModel, 'chef-cookbooks/php');
    }

    public function testAddPublishedAtToModel()
    {
        $githubManager = new GitHubRepositoryComparatorManager($this->gitHubApiConnector);

        $repositoryModel = new GitHubRepositoryModel();

        $githubManager->addPublishedAtToModel($repositoryModel, 'chef-cookbooks/php');

        $this->assertNotNull($repositoryModel->publishedAt);
    }

    /**
     * @expectedException \Application\Manager\Exception\WrongNumberOfRepositoriesToCompare
     */
    public function testCompareRepositoriesAndReturnCompareRepositoriesResultModelThrowsException()
    {
        $githubManager = new GitHubRepositoryComparatorManager($this->gitHubApiConnector);

        $githubManager->addRepository('https://github.com/docker-library/php');

        $githubManager->compareRepositoriesAndReturnCompareRepositoriesResultModel();
    }

    public function testCompareRepositoriesAndReturnCompareRepositoriesResultModel()
    {
        $githubManager = new GitHubRepositoryComparatorManager($this->gitHubApiConnector);

        $githubManager->addRepository('https://github.com/docker-library/php');
        $githubManager->addRepository('chef-cookbooks/php');

        $result = $githubManager->compareRepositoriesAndReturnCompareRepositoriesResultModel();

        $this->assertInstanceOf(CompareRepositoriesResultModel::class, $result);
    }
}

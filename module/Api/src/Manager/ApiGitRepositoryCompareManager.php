<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-22
 * Time: 02:18
 */

namespace Api\Manager;

use Application\Manager\GitHubRepositoryComparatorManager;

class ApiGitRepositoryCompareManager
{
    /**
     * @var GitHubRepositoryComparatorManager
     */
    protected $comparatorManager;

    /**
     * ApiGitRepositoryCompareManager constructor.
     * @param GitHubRepositoryComparatorManager $comparatorManager
     */
    public function __construct(GitHubRepositoryComparatorManager $comparatorManager)
    {
        $this->comparatorManager = $comparatorManager;
    }

    /**
     * @param $repository
     * @throws \Application\Manager\Exception\GetRepositoryException
     * @throws \Application\Manager\Exception\RepositoryNotFoundException
     * @throws \Application\Manager\Exception\TooManyRepositoriesException
     */
    public function addRepository($repository)
    {
        $this->comparatorManager->addRepository($repository);
    }

    /**
     * @return array
     * @throws \Application\Manager\Exception\WrongNumberOfRepositoriesToCompare
     */
    public function compareRepositoriesAndReturnCompareRepositoriesResultModel()
    {
        $comparisonResultArray = [];

        $result = $this->comparatorManager->compareRepositoriesAndReturnCompareRepositoriesResultModel();

        foreach ($result->getRepositoryName() as $index => $name) {
            $comparisonResultArray[$index]['repository_name'] = $name;
        }

        foreach ($result->getRepositoryUrl() as $index => $url) {
            $comparisonResultArray[$index]['repository_url'] = $url;
        }

        foreach ($result->getComparisonArray() as $key => $comparison) {
            foreach ($comparison as $index => $value) {
                $comparisonResultArray[$index][$key] = $value;
            }
        }

        return $comparisonResultArray;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-22
 * Time: 02:19
 */

namespace Api\Manager\Factory;

use Api\Manager\ApiGitRepositoryCompareManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ApiGitRepositoryCompareManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ApiGitRepositoryCompareManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $apiGitRepositoryCompare = new ApiGitRepositoryCompareManager(
            $container->get(\Application\Manager\GitHubRepositoryComparatorManager::class)
        );
        return $apiGitRepositoryCompare;
    }
}

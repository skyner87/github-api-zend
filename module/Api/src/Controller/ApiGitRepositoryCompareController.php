<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApi for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Api\Controller;

use Api\Manager\ApiGitRepositoryCompareManager;
use Application\Manager\GitHubRepositoryComparatorManager;
use Application\Service\GitHubApiConnector\Exception\NotFoundException;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class ApiGitRepositoryCompareController extends AbstractRestfulController
{

    /**
     * @var ApiGitRepositoryCompareManager
     */
    protected $comparatorManager;

    public function __construct(ApiGitRepositoryCompareManager $comparatorManager)
    {
        $this->comparatorManager = $comparatorManager;
    }

    public function getList()
    {
        $firstRepository = $this->params()->fromQuery('first_repository', '');
        $secondRepository = $this->params()->fromQuery('second_repository', '');

        try {
            $this->comparatorManager->addRepository($firstRepository);
            $this->comparatorManager->addRepository($secondRepository);

            $result = $this->comparatorManager->compareRepositoriesAndReturnCompareRepositoriesResultModel();

            return new JsonModel(['status' => 'success', 'comparison_data' => $result]);
        } catch (NotFoundException $exception) {
            $this->response->setStatusCode(404);
            return new JsonModel(['status' => 'error', 'message' => $exception->getMessage()]);
        } catch (\Exception $exception) {
            $this->response->setStatusCode(500);
            return new JsonModel(['status' => 'error', 'message' => 'Please contact with administrator']);
        }
    }
}

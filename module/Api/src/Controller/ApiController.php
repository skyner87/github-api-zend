<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-22
 * Time: 02:54
 */

namespace Api\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class ApiController extends AbstractRestfulController
{
    public function getList()
    {
        return new JsonModel(parent::getList());
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-21
 * Time: 21:10
 */

namespace Api\Controller\Factory;

use Api\Controller\ApiGitRepositoryCompareController;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ApiGitRepositoryCompareControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ApiGitRepositoryCompareController(
            $container->get(\Api\Manager\ApiGitRepositoryCompareManager::class)
        );
        return $controller;
    }
}

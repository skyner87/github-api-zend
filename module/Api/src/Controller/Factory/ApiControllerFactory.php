<?php
/**
 * Created by PhpStorm.
 * User: Skyner
 * Date: 2018-08-22
 * Time: 02:53
 */

namespace Api\Controller\Factory;

use Api\Controller\ApiController;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ApiControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new ApiController();
    }
}

<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApi for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Api;

use Yaf\Exception\LoadFailed\Controller;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'api' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/api',
                    'defaults' => [
                        'controller' => \Api\Controller\ApiController::class,
                        'action'     => null,
                    ],
                ],
            ],
            'api-compare' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/api/compare/repository',
                    'defaults' => [
                        'controller' => \Api\Controller\ApiGitRepositoryCompareController::class,
                        'action'     => null,
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            \Api\Controller\ApiController::class =>
                \Api\Controller\Factory\ApiControllerFactory::class,
            \Api\Controller\ApiGitRepositoryCompareController::class =>
                \Api\Controller\Factory\ApiGitRepositoryCompareControllerFactory::class
        ],
    ],
    'view_manager' => [
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
